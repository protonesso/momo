require_relative 'lib/momo/cli'

m = Momo::Cli.new(ARGV)
m.parseOpts
m.parsePkgs
m.runCommands

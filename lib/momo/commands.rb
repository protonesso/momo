module Momo

def self.runBuild(config)
	puts "Building"
end

def self.runEmerge(packages, rootfs, config)
	puts packages
end

def self.runInfo(packages, rootfs)
	puts packages[0]
end

def self.runInstall(packages, rootfs)
	puts rootfs
	puts packages
end

def self.runRemove(packages, rootfs)
	puts packages
end

def self.runSearch(packages, rootfs, config)
	puts packages[0]
end

def self.runSync()
	puts "Updating"
end

def self.runUpgrade(rootfs, config)
	puts "Updating"
end

end

require 'optparse'

require_relative 'commands'

module Momo
	$gConfig = "/etc/momo.conf"
	$gRootfs = "/"

class Cli
	def initialize(arguments)
		@mArgv = arguments
		@mArg0 = arguments[0]
		@data = nil
	end

	def parsePkgs
		case @mArg0
		when "info"
			@mArgv.shift
			@data = @mArgv[0]
		when "install"
			@mArgv.shift
			@data = @mArgv
		when "remove"
			@mArgv.shift
			@data = @mArgv
		when "search"
			@mArgv.shift
			@data = @mArgv[0]
		end
	end

	def parseOpts
		@mArgv = OptionParser.new do |opts|
			opts.banner = "Usage: momo [subcommand] [options] [packageN]"

			opts.on("-h", "--help", "Prints this message") do
				puts opts
				exit 0
			end
			opts.on("-c", "--config [CONFIG]", "Specify config file path") do |config|
				$gConfig = config
			end
			opts.on("-r", "--rootfs [ROOTFS]", "Specify root filesystem path") do |rootfs|
				$gRootfs = rootfs
			end
			opts.on("-f", "--force", "Specify root filesystem path") do
				$gForce = true
			end
			opts.on("-D", "--no-download", "Do not download source files") do
				$gDownload = false
			end
			opts.on("-V", "--no-verify", "Do not verify source files") do
				$gVerify = false
			end
			opts.on("-E", "--no-extract", "Do not extract tarballs") do
				$gExtract = false
			end
			opts.on("-C", "--no-build", "Do not run build of package") do
				$gCompile = false
			end
			opts.on("-P", "--no-package", "Do not generate binary package") do
				$gGenerate = false
			end
			opts.on("-S", "--no-sign", "Do not sign compiled package") do
				$gSignPkg = false
			end
		end.parse!
	end

	def runCommands
		case @mArg0
		when "build"
			Momo.runBuild($gConfig)
		when "emerge"
			Momo.runEmerge(@mArgv, $gRootfs, $gConfig)
		when "info"
			Momo.runInfo(@mArgv, $gRootfs)
		when "install"
			Momo.runInstall(@mArgv, $gRootfs)
		when "remove"
			Momo.runRemove(@mArgv, $gRootfs)
		when "search"
			Momo.runSearch(@mArgv, $gRootfs, $gConfig)
		when "sync"
			Momo.runSync
		when "upgrade"
			Momo.runUpgrade($gRootfs, $gConfig)
		else
			puts "Unknown action"
			puts "Run `momo help` to get list of available commands"
			exit 1
		end
	end
end

end
